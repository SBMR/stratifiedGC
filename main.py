import numpy as np
from statsmodels.tsa.tsatools import lagmat2ds
from statsmodels.tools.tools import add_constant
from statsmodels.regression.linear_model import OLS
from scipy import stats
from tqdm import tqdm


def gc(x,maxlag=1,number_bin=10, alpha=0.05, min_observation=30):
    """
    Parameters
    ----------
    x : array, 2d
        data for test whether the time series in the second column Granger
        causes the time series in the first column
    maxlag : integer
        the Granger causality test results are calculated for all lags up to
        maxlag
    number_bin : integer
        decide the level of stratification of the test, i.e. how many linear segments are created for approximating the causal function.
    """

    pvalues = []
    dta = lagmat2ds(x, maxlag, trim='both', dropex=1)
    freq, bins = np.histogram(dta[:,-1], bins=number_bin)

    for strat in range(len(bins[:-1])):
        idx = np.where(np.logical_and(dta[:,-1]>=bins[strat], dta[:,-1]<=bins[strat+1]))

        if len(idx[0]) > min_observation:
            dtaf = dta[idx[0],:]
            dtaown = add_constant(dtaf[:, 1:(maxlag + 1)], prepend=False)
            dtajoint = add_constant(dtaf[:, 1:], prepend=False)
            # Run ols on both models without and with lags of second variable
            res2down = OLS(dtaf[:, 0], dtaown).fit()
            res2djoint = OLS(dtaf[:, 0], dtajoint).fit()
            # Granger Causality test using ssr (F statistic)
            fgc1 = ((res2down.ssr - res2djoint.ssr) /
                    res2djoint.ssr / maxlag * res2djoint.df_resid)
            pvalues.append( stats.f.sf(fgc1, maxlag, res2djoint.df_resid) )

    pvalue = np.nanmin(pvalues)
    corrected_alpha = 1 - (1 - alpha)**(1/number_bin)

    return pvalue < corrected_alpha, pvalue, corrected_alpha

